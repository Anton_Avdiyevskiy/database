package com.lviv.dvoretskyi.databasestudents.entity;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class TemplateTrackingResultEmployee {

  @Id
  private int id;
  @ManyToOne
  @JoinColumn(name = "templateTrackingEmployeeId")
  private TemplateTrackingEmployee templateTrackingEmployee;
  private boolean isSkypeSpecified;
  private boolean isMobileSpecified;

  public TemplateTrackingResultEmployee() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public TemplateTrackingEmployee getTemplateTrackingEmployee() {
    return templateTrackingEmployee;
  }

  public void setTemplateTrackingEmployee(
      TemplateTrackingEmployee templateTrackingEmployee) {
    this.templateTrackingEmployee = templateTrackingEmployee;
  }

  public boolean isSkypeSpecified() {
    return isSkypeSpecified;
  }

  public void setSkypeSpecified(boolean skypeSpecified) {
    isSkypeSpecified = skypeSpecified;
  }

  public boolean isMobileSpecified() {
    return isMobileSpecified;
  }

  public void setMobileSpecified(boolean mobileSpecified) {
    isMobileSpecified = mobileSpecified;
  }
}
